-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 10, 2018 at 03:12 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinemaster`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` text,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_accessed` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `name`, `create_time`, `last_accessed`) VALUES
(1, 'rk@gmail.com', '79e386a6b06c4e8804108b87fa62c266', 'rochel', '2018-12-06 16:53:06', '2018-12-10 11:07:59'),
(2, 'roch@gmail.com', 'aa95ce5aa68e77c0c9652d35c3fd1802', 'Rochel', '2018-12-10 15:04:07', '2018-12-10 15:04:07'),
(3, 'roko@gmail.com', '79e386a6b06c4e8804108b87fa62c266', 'Rochel', '2018-12-10 15:08:00', '2018-12-10 15:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `viewings`
--

DROP TABLE IF EXISTS `viewings`;
CREATE TABLE IF NOT EXISTS `viewings` (
  `viewing_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `movie_title` varchar(32) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `number_of_tickets` int(11) NOT NULL,
  PRIMARY KEY (`viewing_id`),
  KEY `USER_FK` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `viewings`
--

INSERT INTO `viewings` (`viewing_id`, `user_id`, `movie_title`, `date`, `time`, `number_of_tickets`) VALUES
(1, 1, 'Creed II', '2018-12-07', '20:00:00', 10),
(2, 1, 'Creed II', '2018-12-11', '11:30:00', 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `viewings`
--
ALTER TABLE `viewings`
  ADD CONSTRAINT `USER_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
