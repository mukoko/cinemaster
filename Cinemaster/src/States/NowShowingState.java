package States;

import HtmlScraping.MovieCollection;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.*;

public class NowShowingState extends State implements IState {
    private static NowShowingState Instance = null;
    private HashMap<String, ArrayList<String>> _moviesShowing;
    private ArrayList<String> _availableDays;
    private ArrayList<String> _orderedKeys;
    private static String _dateAsString;
    private int _currentIndex;
    private int _startIndexOfDays;

    public static void open(String date){
        if (Instance == null){
            Instance = new NowShowingState(date);

            StateManager.getInstance().openState(Instance);
        }
    }

    private NowShowingState(String date){
        _dateAsString = date;
        MovieCollection.getInstance().setViewDate(_dateAsString);
    }
    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Now Showing State: Enter");
        System.out.println("Showing movies for the date: " + _dateAsString);

        fillMoviesShowing();

        outputMoviesShowing();
        outputAvailableDays();
        outputOtherStates();
    }

    private void fillMoviesShowing(){
        _moviesShowing = new HashMap<>();
        _moviesShowing = MovieCollection.getInstance().getMoviesForDate(_dateAsString);
    }

    private void outputMoviesShowing(){
        _currentIndex = 1;
        _orderedKeys = new ArrayList<>();
        for (String key : _moviesShowing.keySet()) {
            System.out.println(_currentIndex + ". " + key + " " + _moviesShowing.get(key).toString());
            _orderedKeys.add(key);
            _currentIndex++;
        }

        System.out.println("");
    }

    private void outputAvailableDays(){
        _startIndexOfDays = _currentIndex;
        _availableDays = new ArrayList<>();

        //Gets the dates for the week, from the Today's date.
        //Adds the dates that is the date you are currently viewing to a list
        for (int i = 0; i < 7; i++){
            DateTime date = DateTime.now().plusDays(i);
            String dateInformation = date.toString(DateTimeFormat.forPattern("dd/MM/yyyy"));

            if (!dateInformation.equals(_dateAsString)){
                _availableDays.add(dateInformation);
                String day = date.dayOfWeek().getAsText();
                System.out.println(_currentIndex + ". " + day);
                _currentIndex++;
            }
        }

        System.out.println("");
    }

    private void outputOtherStates(){

        System.out.println(_currentIndex + ". Trailers");
        _currentIndex++;
        System.out.println(_currentIndex + ". Account");
        _currentIndex++;
        System.out.println(_currentIndex + ". Logout");
    }


    @Override
    public void update(StateManager stateManager) {
        updateIntSequence();
    }

    @Override
    protected boolean validInput(int input){
        return input >= 1 && input <= _currentIndex;
    }

    @Override
    protected void processInput(int number){
        if (number < _startIndexOfDays){
            MovieSynopsisState.open(_orderedKeys.get(number - 1));
        }
        else if (number < _startIndexOfDays + 6){
            //Re-loads the state with a different date
            NowShowingState.close();
            NowShowingState.open(_availableDays.get(number - _startIndexOfDays));
        }
        else{
            int no = _currentIndex - number + 1;
            switch (no){
                case 1:
                    logout();
                    break;
                case 2:
                    AccountState.open();
                    break;
                case 3:
                    StateManager.getInstance().closeAllStates();
                    TrailersState.open();
                    break;
            }
        }
    }

    @Override
    public void resume() {
        System.out.println(" ");
        System.out.println("Now Showing State: Resume");
        System.out.println("Showing movies for the date: " + _dateAsString);

        outputMoviesShowing();
        outputAvailableDays();
        outputOtherStates();
        _receivedInput = false;
    }

    @Override
    public void exit() {
        System.out.println("Now Showing State: Exit");
    }

    @Override
    public String getName() {
        return "Now Showing";
    }

    @Override
    public void destroy() {
        Instance = null;
    }
}
