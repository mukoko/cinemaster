package States;

import Backbone.Database;
import Backbone.Encryption;
import Backbone.Validation;
import HtmlScraping.MovieCollection;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SignUpState extends State implements IState{
    private static SignUpState Instance = null;

    private String _email;
    private String _password;
    private String _name;

    public static void open(){ //Could do open(parameter)
        if (Instance == null){
            Instance = new SignUpState();

            StateManager.getInstance().openState(Instance);
        }
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Sign Up State: Enter");
        System.out.println("Enter 'exit' at anytime to exit");
        System.out.println("Enter your email");
    }

    @Override
    public void update(StateManager stateManager) {
        updateStringSequence();
    }

    @Override
    protected boolean validInput(String entry){
        return true;
    }

    @Override
    protected void processInput(String entry){
        if (entry.toLowerCase().equals("exit")){
            close();
            EntryState.open();
            return;
        }

        if (_email == null){
            _email = entry;
            System.out.println("Enter your password");
            _receivedInput = false;
        }
        else if (_password == null){
            _password = entry;
            System.out.println("Enter your name");
            _receivedInput = false;
        }
        else if (_name == null){
            _name = entry;
            _receivedInput = false;

            boolean valid = ValidDetails();

            if (valid){
                _password = Encryption.getPassword(_password);
                int result = Database.getInstance().insertNewUser(_email, _password, _name);
                if (result == 0){
                    System.out.println("An account with the email " + _email + " already exists");
                    valid = false;
                }
                else if (result == -1){
                    System.out.println("An unknown error occurred when attempting to make your account");
                    valid = false;
                }
                else if (result == 1){
                    _receivedInput = true;
                    System.out.println("Your account was successfully created");
                    close();
                    EntryState.open();
                    return;
                }
            }

            if (!valid){
                System.out.println("Enter a email");
                _email = null;
                _password = null;
                _name = null;
            }
        }
    }

    private boolean ValidDetails(){
        boolean valid = true;

        if (!Validation.nameValidation(_name, 3, 32)){
            System.out.println("The name you have entered is not valid");
            System.out.println("It must contain atleast 3 letters");
            valid = false;
        }
        if (!Validation.emailValidation(_email, 6, 32)) {
            System.out.println("The email you have entered is not in a valid format");
            valid = false;
        }
        if (!Validation.passwordValidation(_password, true, true, true, 6, 32)){

            System.out.println("The password you have entered is not valid");
            System.out.println("It must be atleast 6 characters");
            System.out.println("It muse contains a capital letter, a number and a special character *[!#$%&'*+/=?`{|}~^]");
            valid = false;
        }

        return valid;
    }

    @Override
    public void resume() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void destroy() {
        Instance = null;
    }

    @Override
    public String getName() {
        return "Sign Up";
    }
}
