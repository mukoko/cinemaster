package States;

import HtmlScraping.Movie;
import HtmlScraping.MovieCollection;

import java.util.ArrayList;

public class TrailersState extends State implements IState {
    private static TrailersState Instance = null;
    private ArrayList<Movie> _movies;
    private int _currentIndex;

    public static void open(){
        if (Instance == null){
            Instance = new TrailersState();

            StateManager.getInstance().openState(Instance);
        }
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Trailers State: Enter");
        System.out.println("Enter a number to view the trailer");
        _movies = MovieCollection.getInstance().getMovies();
        _currentIndex = 1;

        for (Movie movie : _movies) {
            System.out.println(_currentIndex + ". " + movie.getTitle());
            _currentIndex++;
        }

        System.out.println("");
        System.out.println(_currentIndex + ". Now Showing");
        _currentIndex++;
        System.out.println(_currentIndex + ". Account");
        _currentIndex++;
        System.out.println(_currentIndex + ". Logout");
    }

    @Override
    public void update(StateManager stateManager) {
        updateIntSequence();
    }

    @Override
    protected boolean validInput(int number) {
        return number >= 1 && number <= _currentIndex;
    }

    @Override
    protected void processInput(int number) {
        if (number <= _movies.size()){
            Movie movie = _movies.get(number - 1);
            showTrailer(movie.getTrailerUrl(), movie.getTitle());
            _receivedInput = false;
        }
        else {
            switch (_currentIndex - number + 1){
                case 1:
                    logout();
                    break;
                case 2:
                    close();
                    AccountState.open();
                    break;
                case 3:
                    close();
                    NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
                    break;
            }
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void exit() {
        exitMedia();
    }

    @Override
    public void destroy() {
        Instance = null;
    }

    @Override
    public String getName() {
        return "Trailers";
    }
}
