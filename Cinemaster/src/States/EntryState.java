package States;

import HtmlScraping.MovieCollection;

public class EntryState extends State implements IState {
    private static EntryState Instance;

    public static void open(){ //Could do open(parameter)
        if (Instance == null){
            Instance = new EntryState();

            StateManager.getInstance().openState(Instance);
        }
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Entry State: Enter");
        System.out.println("Welcome enter one of the following numbers to continue,");
        printOutput();
    }

    private void printOutput(){
        System.out.println("1. Login");
        System.out.println("2. Sign Up");
        System.out.println("3. Continue without logging in");
    }

    @Override
    public void update(StateManager stateManager) {
        updateIntSequence();
    }

    @Override
    protected boolean validInput(int number){
        return number == 1 || number == 2 || number == 3;
    }

    @Override
    protected void processInput(int number){
        EntryState.close();
        switch (number){
            case 1:
                LoginState.open();
                break;
            case 2:
                SignUpState.open();
                break;
            case 3:
                NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
                break;
        }
    }

    @Override
    protected void invalidInput(){
        super.invalidInput();
        printOutput();
    }

    @Override
    public void resume() {
        System.out.println("Entry State: Resume");
    }

    @Override
    public void exit() {
        System.out.println("Entry State: Exit");
    }

    @Override
    public String getName() {
        return "Entry";
    }

    @Override
    public void destroy(){
        Instance = null;
    }
}
