package States;

import java.util.Stack;

/*
Controls the movement between states, and updating the current state
 */
public class StateManager {
    private static StateManager Instance = null;
    private Stack<IState> _stateStack = new Stack<>();

    public static StateManager getInstance(){
        if (Instance == null)
            Instance = new StateManager();

        return Instance;
    }

    private StateManager(){
    }

    public void destroy() {
        Instance = null;
    }

    protected void openState(IState state){
        if (_stateStack.size() > 0){
            if (_stateStack.peek() == state)
                return;
        }

        System.out.println("Opening State: " + state.getName());
        _stateStack.push(state);
        //state.init();
        state.enter();
    }

    public void closeState(IState state){
        if (_stateStack.size() == 0) return;
        if (_stateStack.peek() != state) return;

        System.out.println("Closing State: " + state.getName());
        closeTop();
    }

    private void closeTop(){
        IState state = _stateStack.pop();
        state.exit();
        state.destroy();

        if (_stateStack.size() > 0)
            _stateStack.peek().resume();
    }

    protected void closeAllStates(){
        if (_stateStack.size() == 0) return;

        _stateStack.peek().exit();
        for (IState state: _stateStack) {
            state.destroy();
        }

        System.out.println("Clearing All States");
        _stateStack.clear();
    }

    public void update(){ if (_stateStack.size() > 0) _stateStack.peek().update(this);}

    public Stack<IState> getStateStack() {
        return _stateStack;
    }
}
