package States;

import Account.UserAccount;
import Backbone.Database;
import HtmlScraping.Movie;
import HtmlScraping.MovieCollection;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class BookingState extends State implements IState{
    private static BookingState Instance = null;
    private Movie _movie;
    private String _day;
    private ArrayList<String> _viewingTimes;

    protected int _validInputRange;

    private int _maxTickets = 20;

    private int _viewingTimeIndex = -1;
    private int _numberOfTickets = -1;

    public static void open(Movie movie){
        if (Instance == null){
            Instance = new BookingState(movie);

            StateManager.getInstance().openState(Instance);
        }
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    protected BookingState(Movie movie){
        _movie = movie;
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Booking State: Enter");

        if (UserAccount.getInstance() == null){
            System.out.println("User not logged in");
            StateManager.getInstance().closeAllStates();
            AccountState.open();
            return;
        }

        _day = MovieCollection.getInstance().getViewDate();
        _viewingTimes = _movie.getMovieDate(_day).getShowtimes();

        System.out.println("Book a viewing for the movie " + _movie.getTitle() + " on " + _day + "\n");

        System.out.println("Choose a time");
        for (int i = 1; i <= _viewingTimes.size(); i++)
            System.out.println(i + ". " + _viewingTimes.get(i - 1));

        _validInputRange = _viewingTimes.size();

    }

    @Override
    public void update(StateManager stateManager) {
        updateStringSequence();
    }


    @Override
    protected boolean validInput(String entry) {
        if (entry.equals("exit")) return true;
        if (entry.matches("-?\\d+(\\.\\d+)?")){
            int i = Integer.parseInt(entry);
            return i >= 1 && i <= _validInputRange;
        }
        return false;
    }

    @Override
    protected void processInput(String entry) {
        if (entry.equals("exit")){
            close();
            return;
        }

        if (_viewingTimeIndex == -1){
            _viewingTimeIndex = Integer.parseInt(entry);
            _validInputRange = _maxTickets;
            System.out.println("Enter the number of tickets you wish to book");
            _receivedInput = false;
        }
        else if (_numberOfTickets == -1){
            _numberOfTickets = Integer.parseInt(entry);
            int noViewingsForMovie = Database.getInstance().getMovieViewingCount(_movie.getTitle());
            int ticketsRemaining = Database.getInstance().getViewingCapacity() - noViewingsForMovie;
            int reachedCap = Database.getInstance().getViewingCapacity() - (noViewingsForMovie + _numberOfTickets);


            if (ticketsRemaining <= 0 || reachedCap < 0){
                if (ticketsRemaining <= 0) System.out.println("There are no more tickets left for the movie");
                else System.out.println("There is only " + ticketsRemaining + " ticket(s) remaining");

                _viewingTimeIndex = -1;
                _numberOfTickets = -1;
                _receivedInput = false;
            }
            else {
                String time = _viewingTimes.get(_viewingTimeIndex - 1);
                int i = Database.getInstance().insertViewingEntry(UserAccount.getInstance().getEmail(), _movie.getTitle(),
                        _day, time, _numberOfTickets);

                _receivedInput = false;

                if (i != 1){
                    System.out.println("There was an error booking your ticket, please start again");
                    _viewingTimeIndex = -1;
                    _numberOfTickets = -1;
                    return;
                }

                System.out.println("Your ticket has been successfully booked, enter 'exit' to exit");
            }
            //... id, user_id, movie title, date, time, no of tickets.
        }
    }

    @Override
    public void resume() {
        System.out.println("Booking State: Resume");

    }

    @Override
    public void exit() {
        System.out.println("Booking State: Exit");

    }

    @Override
    public void destroy() {

    }

    @Override
    public String getName() {
        return "Booking";
    }
}
