package States;

//Each state implements the Interface IState
public interface IState {
    //void init();
    void enter();
    void update(StateManager stateManager);
    void resume();
    void exit();
    void destroy();
    String getName();
}
