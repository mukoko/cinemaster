package States;

import Account.UserAccount;
import Backbone.Database;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class State {
    protected boolean _receivedInput;

    protected static ArrayList<JFrame> _frames = new ArrayList<>();
    protected static ArrayList<WebView> _webViews = new ArrayList<>();

    //Dimensions for JFrame opened when opening a trailer
    private static int _width = 700;
    private static int _height = 460;

    //Update Sequenece ran when we need to input a number
    protected void updateIntSequence() {
        if (!_receivedInput) {
            int number;
            System.out.println(".");

            try {
                Scanner input = new Scanner(System.in);
                number = input.nextInt();
            } catch (InputMismatchException e) {
                invalidInput();
                return;
            }

            if (validInput(number)) {
                _receivedInput = true;
                processInput(number);
            } else invalidInput();
        }
    }

    //Update sequence for strings
    protected void updateStringSequence(){
        if (!_receivedInput){
            String entry;
            System.out.println(".");

            try {
                Scanner scanner = new Scanner(System.in);
                entry = scanner.nextLine();
            } catch (InputMismatchException e){
                invalidInput();
                return;
            }

            if (validInput(entry)){
                _receivedInput = true;
                processInput(entry);
            } else invalidInput();
        }
    }

    //Valid Input should return true if the input is valid
    //Proccess Input operations that take place onto the valid input
    protected boolean validInput(String entry){ return false; }
    protected void processInput(String entry){}

    protected boolean validInput(int number){ return false; }
    protected void processInput(int number){}

    protected void invalidInput(){
        System.out.println("Your input was invalid");
        System.out.println("Please enter something valid");
    }

    protected void logout(){
        StateManager.getInstance().closeAllStates();
        UserAccount.logout();
        Database.getInstance().closeConnections();
        EntryState.open();
    }

    //When we exit a state with a trailer open
    protected void exitMedia(){
        if (_frames.size() == 0) return;

        for (int i = _frames.size() - 1; i >= 0; i--)
            _frames.get(i).dispose();

        Platform.runLater(()->{
            for (WebView webView : _webViews) {
                webView.getEngine().load(null);
            }
        });

        _webViews.clear();
    }

    protected static void showTrailer(String trailerUrl, String movieTitle){
        showMedia(trailerUrl, movieTitle, _width, _height);
    }

    //Opens up a JFrame and displays a movie trailer
    protected static void showMedia(String trailerUrl, String movieTitle, int width, int height){
        try{
            System.out.println("Opening Trailer");
            JFrame frame = new JFrame(movieTitle + " Trailer");
            _frames.add(frame);

            JFXPanel jfxPanel = new JFXPanel();
            //CountDownLatch countDownLatch = new CountDownLatch(1);
            Platform.setImplicitExit(false);
            Platform.runLater(() -> {
                WebView webView = new WebView();
                webView.getEngine().load(trailerUrl);
                webView.setPrefSize(width, height);
                _webViews.add(webView);
                jfxPanel.setScene(new Scene(webView));

                //When we close the frame
                frame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        System.out.println("Closing Trailer");
                        Platform.runLater(()->{
                            //Loads a blank page to stop the sound
                            webView.getEngine().load(null);
                        });
                        _frames.remove(frame);
                        frame.dispose();
                        //countDownLatch.countDown();
                    }
                });
                frame.add(new JScrollPane(jfxPanel));
                frame.setVisible(true);
                frame.pack();
                frame.setFocusable(true);
                frame.setLocationRelativeTo(null);
            });
            //countDownLatch.await();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("There was a error displaying the trailer");
        }

        System.out.println("Receive input");
    }
}
