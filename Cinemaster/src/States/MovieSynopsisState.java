package States;

import HtmlScraping.Movie;
import HtmlScraping.MovieCollection;


public class MovieSynopsisState extends State implements IState {
    private static MovieSynopsisState Instance = null;
    private String _movieTitle;
    private Movie _movie;

    public static void open(String movieTitle){
        if (Instance == null){
            Instance = new MovieSynopsisState(movieTitle);

            StateManager.getInstance().openState(Instance);
        }
    }

    protected MovieSynopsisState(String movieTitle){
        _movieTitle = movieTitle;
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Synopsis State: Enter");
        if (_movieTitle == null) MovieSynopsisState.close();
        System.out.println("Displaying Information for the Movie: " + _movieTitle + "\n");

        _movie = MovieCollection.getInstance().findMovie(_movieTitle);

        if (_movie == null) {
            MovieSynopsisState.close();
            return;
        }

        output();
    }

    private void output(){
        System.out.println("Displaying Information for the Movie: " + _movieTitle + "\n");
        System.out.println("Title: " + _movie.getTitle());
        System.out.println("Description: " + _movie.getDescription());
        System.out.println(_movie.getClassification());
        System.out.println(_movie.getRunningTime());
        System.out.println("Warnings: " + _movie.getWarnings() + "\n");

        System.out.println("1. Poster");
        System.out.println("2. Trailer" + "\n");

        System.out.println("3. Book Now!" + "\n");

        System.out.println("4. Back");
        System.out.println("5. Now Showing");
        System.out.println("6. Trailers");
        System.out.println("7. Account");
        System.out.println("8. Logout");
    }

    @Override
    public void update(StateManager stateManager) {
        updateIntSequence();
    }

    @Override
    protected boolean validInput(int number){
        return number >= 1 && number <= 8;
    }

    @Override
    protected void processInput(int number){
        switch (number){
            case 1:
                showMedia(_movie.getImageUrl(), _movie.getTitle(), 83, 128);
                _receivedInput = false;
                break;
            case 2:
                showTrailer(_movie.getTrailerUrl(), _movie.getTitle());
                _receivedInput = false;
                break;
            case 3:
                BookingState.open(_movie);
                break;
            case 4:
                close();
                //Return back to the Now Showing State for this day
                break;
            case 5:
                StateManager.getInstance().closeAllStates();
                NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
                break;
            case 6:
                StateManager.getInstance().closeAllStates();
                TrailersState.open();
                break;
            case 7:
                AccountState.open();
                break;
            case 8:
                logout();
                break;
        }
    }

    @Override
    public void resume() {
        System.out.println(" ");
        System.out.println("Movie Synopsis: Resume");
        output();

        _receivedInput = false;
    }

    @Override
    public void exit() {
        exitMedia();
    }

    @Override
    public void destroy() {
        Instance = null;
    }

    @Override
    public String getName() {
        return "Movie Synopsis";
    }
}
