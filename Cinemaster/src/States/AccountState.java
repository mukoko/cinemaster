package States;

import Account.UserAccount;
import Backbone.Database;
import HtmlScraping.MovieCollection;

import java.util.ArrayList;
import java.util.HashMap;

public class AccountState extends State implements IState {
    private static AccountState Instance;

    public static void open(){ //Could do open(parameter)
        if (Instance == null){
            Instance = new AccountState();

            StateManager.getInstance().openState(Instance);
        }
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Account State: Enter");
        if (UserAccount.getInstance() == null){
            System.out.println("No user detected");
            StateManager.getInstance().closeAllStates();
            EntryState.open();
            return;
        }

        System.out.println("Account Email: " + UserAccount.getInstance().getEmail());
        System.out.println("Account user's Name: " + UserAccount.getInstance().getName());
        System.out.println("Account Creation Date: " + UserAccount.getInstance().getCreateTime());
        System.out.println("Account Last Access Date: " + UserAccount.getInstance().getLastAccessed() + "\n");

        ArrayList<HashMap<String, Object>> viewings = Database.getInstance().getViewings(UserAccount.getInstance().getEmail());
        if (viewings.size() == 0)
            System.out.println("You have no recent bookings");
        else{
            System.out.println("Your recent bookings: ");
            for (HashMap viewing : viewings) {
                System.out.println("Movie Title: " + viewing.get("Title").toString() + " Date: " + viewing.get("Date").toString()
                + " Time: " + viewing.get("Time").toString() + " Number of Tickets Booked: " + viewing.get("Tickets Count").toString());
            }
        }

        System.out.println("");
        System.out.println("1. Now Showing");
        System.out.println("2. Trailers");
        System.out.println("3. Logout");
    }

    @Override
    public void update(StateManager stateManager) {
        updateIntSequence();
    }

    @Override
    protected boolean validInput(int number) {
        return number >= 1 && number <= 3;
    }

    @Override
    protected void processInput(int number) {
        switch (number){
            case 1:
                close();
                NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
                break;
            case 2:
                close();
                TrailersState.open();
                break;
            case 3:
                logout();
                break;
        }
    }

    @Override
    public void resume() {
        System.out.println("Account State: Exit");
    }

    @Override
    public void exit() {
        System.out.println("Account State: Exit");
    }

    @Override
    public void destroy() {
        Instance = null;
    }

    @Override
    public String getName() {
        return "Account";
    }


}
