package States;

import Backbone.Database;
import Backbone.Encryption;

import HtmlScraping.MovieCollection;

public class LoginState extends State implements IState {
    private static LoginState Instance = null;

    private String _email;
    private String _password;

    public static void open(){ //Could do open(parameter)
        if (Instance == null){
            Instance = new LoginState();

            StateManager.getInstance().openState(Instance);
        }
    }

    public static void close(){
        StateManager.getInstance().closeState(Instance);
    }

    @Override
    public void enter() {
        System.out.println("");
        System.out.println("Login State: Enter");
        System.out.println("Enter 'exit' at anytime to exit");
        System.out.println("Enter your email");
    }

    @Override
    public void update(StateManager stateManager) {
        updateStringSequence();
    }

    @Override
    protected boolean validInput(String entry){
        return true;
    }

    @Override
    protected void processInput(String entry){
        if (entry.equals("exit")){
            close();
            EntryState.open();
            return;
        }

        if (_email == null){
            _email = entry;
            System.out.println("Enter your password");
            _receivedInput = false;
        }
        else if (_password == null){
            _password = entry;
            _password = Encryption.getPassword(_password);
            if (Database.getInstance().userLogin(_email, _password)){
                close();
                NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
            }
            else {
                System.out.println("The information you entered was incorrect");
                System.out.println("Enter your email");
                _email = null;
                _password = null;
                _receivedInput = false;
            }
        }
    }

    @Override
    public void resume() {
        System.out.println("Login State: Resume");
    }

    @Override
    public void exit() {
        System.out.println("Login State: Exit");
    }

    @Override
    public void destroy() {
        Instance = null;
    }

    @Override
    public String getName() {
        return "Login";
    }
}
