package HtmlScraping;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;

/*
Holds all of the Movie objects that has been created during the running of the Project, so that we do not have to WebScrape each time we want info on a movie
 */
public class MovieCollection {
    private static MovieCollection Instance = null;
    private int _datesCapacity = 7;
    private ArrayList<Movie> _movies = new ArrayList<>();
    private ArrayList<String> _dates = new ArrayList<>(_datesCapacity);

    private String _viewDate;

    public ArrayList<Movie> getMovies() {
        return _movies;
    }

    private String _pathPrefix = "http://www.empirecinemas.co.uk/?page=nowshowing&tbx_site_id=44&scope=";

    public static MovieCollection getInstance(){
        if (Instance == null)
            Instance = new MovieCollection();

        return Instance;
    }

    public Movie findMovie(String title){
        for (Movie movie : _movies) {
            if (movie.getTitle().equals(title)) return movie;
        }

        return null;
    }

    public HashMap<String, ArrayList<String>> getMoviesForDate(String date){
        HashMap<String, ArrayList<String>> moviesShowing = new HashMap<>();
        addMoviesForDate(date);

        for (Movie movie : _movies) {
            Movie.MovieDate movieDate = movie.getMovieDate(date);
            if (movieDate != null)
                moviesShowing.put(movie.getTitle(), movieDate.getShowtimes());
        }

        return moviesShowing;
    }

    public void addMoviesForDate(String date){
        if (_dates.contains(date)) return;

        //Day before
        if (getDateTime(date).isBefore(new DateTime().withTimeAtStartOfDay()))
            return;

        //Exactly a week
        if (getDateTime(date).isAfter(new DateTime().withTimeAtStartOfDay().plusDays(6)))
            return;

        WebScrape scrape = new WebScrape(_pathPrefix, date);
        scrape.getPageElements();
        scrape.createMovies();
        scrape.setMovieClassifications();
        scrape.setMovieTimes();

        _dates.add(date);
    }

    public String getTodaysDate(){
        DateTime dateTime = new DateTime(DateTime.now());
        DateTimeFormatter dt = DateTimeFormat.forPattern("dd/MM/yyyy");
        return dateTime.toString(dt);
    }

    public DateTime getDateTime(String date){
        return new DateTime(DateTime.parse(date, DateTimeFormat.forPattern("dd/MM/yyyy")));
    }

    public String getViewDate(){
        return _viewDate;
    }

    public void setViewDate(String viewDate) {
        _viewDate = viewDate;
    }
}
