package HtmlScraping;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
This class inspects the HTML tags of the EmpireCinema web page and returns the information we require for this booking system
 */
public class WebScrape {
    private String _url;
    private String _date;
    private HtmlPage _page;
    private ArrayList<Movie> _currentMovies;

    public WebScrape(String url, String date){
        if (url.isEmpty() || date.isEmpty()) throw new IllegalArgumentException();

        _url = url + date;
        _date = date;
        _currentMovies = new ArrayList<>();
    }

    protected void getPageElements(){
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        try {
            _page = client.getPage(_url);
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    protected void createMovies(){
        //Finds each div element that has the class poster
        List<HtmlElement> elements = _page.getByXPath("//div[@class='poster']");
        String trailerUrl = null;
        if (!elements.isEmpty())
        {
            //Get the first img element that is within the div element
            for (HtmlElement element : elements) {
                HtmlImage image = element.getFirstByXPath(".//img");

                boolean movieInCollection = false;
                //This image elemt contains the title of the movie in its alt value.
                //We check if the value of the alt is a movie that we have already found
                for (Movie movie : MovieCollection.getInstance().getMovies()) {
                    if (movie.getTitle().equals(image.getAltAttribute()))
                        movieInCollection = true;
                }

                if (movieInCollection) continue;

                List<HtmlAnchor> ancList = element.getByXPath(".//a");
                for (HtmlAnchor anchor : ancList){
                    if (anchor.getHrefAttribute().contains("xml"))
                        trailerUrl = anchor.getHrefAttribute();
                }

                //Add the movie to the current movies list
                _currentMovies.add(new Movie(image.getAltAttribute(),
                        image.getAttribute("data-content"),
                        _date, image.getSrcAttribute(), trailerUrl));
            }
        }
        else throw new IllegalStateException();
    }

    protected void setMovieClassifications(){
        List<HtmlElement> elements = _page.getByXPath("//div[@class='infos']");
        if (!elements.isEmpty() && _currentMovies.size() != 0) {
            for (HtmlElement element : elements){
                HtmlElement header = element.getFirstByXPath(".//a");
                for (Movie movie : _currentMovies) {
                    if (header.getAttribute("title").contains(movie.getTitle())){
                        HtmlImage image = element.getFirstByXPath(".//img");
                        movie.setClassification(image.getAltAttribute());

                        HtmlElement runtime = element.getFirstByXPath(".//div[@class='runningTime']");
                        String[] info = runtime.asText().split("\\r?\\n");
                        movie.setRunningTime(info[0]);
                        movie.setWarnings(info[1]);

                        break;
                    }
                }
            }

            MovieCollection.getInstance().getMovies().addAll(_currentMovies);
        }
        else throw new IllegalStateException();
    }

    protected void setMovieTimes(){
        List<HtmlElement> elements = _page.getByXPath("//div[@class='showtimes']");
        if (!elements.isEmpty()){
            for (HtmlElement element : elements){
                List<HtmlAnchor> anchors = element.getByXPath(".//a");
                for (Movie movie : MovieCollection.getInstance().getMovies()) {
                    if (anchors.get(0).getAttribute("title").contains(movie.getTitle())){
                        Movie.MovieDate mDate = movie.addShowdate(_date);
                        if (mDate != null){
                            ArrayList<String> datesAsString = new ArrayList<>();
                            for (HtmlAnchor anchor : anchors) {
                                datesAsString.add(anchor.asText());
                            }

                            mDate.addTimes(datesAsString);
                        }
                        break;
                    }
                }
            }
        }
    }

    protected ArrayList<Movie> getMovies() {
        return _currentMovies;
    }

    protected HtmlPage getPage(){
        return  _page;
    }

    protected String getDate() {
        return _date;
    }

    protected String getUrl() {

        return _url;
    }
}
