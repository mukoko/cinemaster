package HtmlScraping;

import java.util.ArrayList;
import java.util.List;

public class Movie {

    public class MovieDate{
        private String _showdate;
        private ArrayList<String> _showtimes;

        public ArrayList<String> getShowtimes() {
            return _showtimes;
        }

        public String getShowdate() {
            return _showdate;
        }

        public MovieDate(String date){
            _showdate = date;
            _showtimes = new ArrayList<>();
        }

        public void addTime(String showtime){
            if (!_showtimes.contains(showtime))_showtimes.add(showtime);
        }

        public void addTimes(List<String> showtimes){
            _showtimes.addAll(showtimes);
        }
    }

    private String _title;
    private String _description;
    private String _classification;
    private String _imageUrl;
    private String _trailerUrl;
    private String _runningTime;
    private String _warnings;
    private ArrayList<MovieDate> _dates;

    private String _trailerPrefix = "http://www.empirecinemas.co.uk";

    public Movie(String title, String description, String date, String url, String trailerUrl){
        _title = title; _description = description; _imageUrl = url;
        _trailerUrl = _trailerPrefix + trailerUrl;
        _dates = new ArrayList<>();
    }

    public String getTitle() {
        return _title;
    }

    public String getDescription() {
        return _description;
    }

    public String getClassification() {
        return _classification;
    }

    public void setClassification(String classification) {
        _classification = classification;
    }

    public String getImageUrl() {
        return _imageUrl;
    }

    public String getTrailerUrl() {
        return _trailerUrl;
    }

    public String getRunningTime() {
        return _runningTime;
    }

    public void setRunningTime(String runningTime) {
        _runningTime = runningTime;
    }

    public String getWarnings() {
        return _warnings;
    }

    public void setWarnings(String warnings) {
        _warnings = warnings;
    }

    public ArrayList<MovieDate> getShowdates() {
        return _dates;
    }

    public MovieDate addShowdate(String date){
        if (getMovieDate(date) == null){
            MovieDate movieDate = new MovieDate(date);
            _dates.add(movieDate);
            return movieDate;
        }

        return null;
    }

    public MovieDate getMovieDate(String date){
        if (_dates.size() == 0) return null;
        for (MovieDate d: _dates) {
            if (d._showdate.equals(date)) return d;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Title: " + _title + "\n" +  "Description: " + _description + "\n" +
                _classification + "\n" + "Image Source: " + _imageUrl + "\n" +
                "Trailer Source: " + _trailerUrl + "\n";
    }
}
