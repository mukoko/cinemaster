package Backbone;
import Account.UserAccount;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

/*
Singleton class that provides a connection to a MySQL database,
class also contains all the queries that are required for the booking system
 */
public class Database {
    private static Database Instance = null;
    private Connection _connection = null;
    private Statement _statement = null;
    private PreparedStatement _preparedStatement = null;
    private ResultSet _resultSet = null;

    //The maximum no. of tickets that can be booked for a viewing
    private int _viewingCapacity = 120;
    public int getViewingCapacity() {
        return _viewingCapacity;
    }

    public static Database getInstance() {
        if (Instance == null)
            Instance = new Database();

        return Instance;
    }

     private Database(){
        try{
            _connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/cinemaster" +
                        "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false","root",""
            );
            _statement = _connection.createStatement();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    //TODO at this moment this method does not take into account the Date and time of the viewing of said movie... not doing what its supposed to do
    //This method returns the total number of tickets that have been booked for a movie
    public int getMovieViewingCount(String movieTitle){
        int numberOfTickets = 0;

        try{
            _resultSet = _statement.executeQuery("select number_of_tickets from cinemaster.viewings where movie_title = " + "'" + movieTitle + "'");
            if (_resultSet.next())
                numberOfTickets += _resultSet.getInt(1);
        }catch (SQLException e){
            e.printStackTrace();
        }

        return numberOfTickets;
    }


    public int insertViewingEntry(String email, String movieTitle, String day, String time, int numberOfTickets){
        int id = -1;
        try {
            _resultSet = _statement.executeQuery("select user_id from cinemaster.users where email = " + "'" + email + "'");
            if (_resultSet.next())
                id = _resultSet.getInt(1);

            if (id != -1){
                _preparedStatement = _connection.prepareStatement("insert into cinemaster.viewings values (default, ?, ?, ?, ?, ?)");

                _preparedStatement.setInt(1, id);
                _preparedStatement.setString(2, movieTitle);

                int d = Integer.parseInt(day.substring(0,2));
                int m = Integer.parseInt(day.substring(3,5));
                int y = Integer.parseInt(day.substring(6));
                _preparedStatement.setDate(3, Date.valueOf(LocalDate.of(y, m, d)));

                int hour = Integer.parseInt(time.substring(0, 2));
                int min = Integer.parseInt(time.substring(3));
                _preparedStatement.setTime(4, Time.valueOf(java.time.LocalTime.of(hour, min)));

                _preparedStatement.setInt(5, numberOfTickets);

                return _preparedStatement.executeUpdate();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return -1;
    }

    public ArrayList<HashMap<String, Object>> getViewings(String email){
        ArrayList<HashMap<String, Object>> viewings = new ArrayList<>();
        try{
            int id = -1;
            _resultSet = _statement.executeQuery("select user_id from cinemaster.users where email = " + "'" + email + "'");
            if (_resultSet.next())
                id = _resultSet.getInt(1);

            if (id != -1){
                _resultSet = _statement.executeQuery("select movie_title, date, time, number_of_tickets from cinemaster.viewings where user_id = " + "'" + id +"'");
                while (_resultSet.next()){
                    HashMap<String, Object> info = new HashMap<>();
                    info.put("Title", _resultSet.getString(1));
                    info.put("Date", _resultSet.getDate(2));
                    info.put("Time", _resultSet.getTime(3));
                    info.put("Tickets Count", _resultSet.getInt(4));
                    viewings.add(info);
                    System.out.println("Info added");
                }
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        return viewings;
    }

    public int insertNewUser(String email, String password, String name){
        try{
            _resultSet = _statement.executeQuery("select email from cinemaster.users where email = " + "'" + email + "'");
            if (_resultSet.next())
                return 0;

            _preparedStatement = _connection.prepareStatement("insert into cinemaster.users values (default, ?, ?, ?, default, default )");

            _preparedStatement.setString(1, email);
            _preparedStatement.setString(2, password);
            _preparedStatement.setString(3, name);

            return _preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }

        return -1;
    }

    public boolean userLogin(String email, String password){
        try{
            _resultSet = _statement.executeQuery("select name, create_time from cinemaster.users where email = " + "'" + email + "'"  + " and password = " + "'" + password + "'");
            String lastAccessed = DateTime.now().withTime(LocalTime.now()).toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
            if (_resultSet.next()){
                UserAccount.login(email, _resultSet.getString(1), _resultSet.getString(2), lastAccessed);
                updateLastAccess(email);
                return true;
            }
        } catch (SQLException e ){
            e.printStackTrace();
        }

        return false;
    }

    protected int updateLastAccess(String email){
        try {
            return _statement.executeUpdate("update cinemaster.users set last_accessed = CURRENT_TIMESTAMP where email = " + "'" + email + "'" );
        }catch (SQLException e){
            e.printStackTrace();
        }

        return -1;
    }

    public void closeConnections(){
        try{
            _connection.close();
            Instance = null;
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
