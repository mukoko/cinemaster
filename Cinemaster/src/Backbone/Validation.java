package Backbone;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    //Checks email is in format chars @ chars . chars
    public static boolean emailValidation(String name, int minLength, int maxLength){
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);

        if(matcher.matches()){
            return name.length() <= maxLength && name.length() >= minLength;
        }

        return false;
    }

    //Checks name is all letters
    public static boolean nameValidation(String name, int minLength, int maxLength){
        String regex = "^[a-zA-Z ]+$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);

        if(matcher.matches()){
            return name.length() <= maxLength && name.length() >= minLength;
        }

        return false;
    }

    //Checks password has a special character, capital letter and number (if respective boolean is true)
    public static boolean passwordValidation(String password, boolean forceSpecialChar, boolean forceCapitalLetter, boolean forceNumber, int minLength, int maxLength){
        StringBuilder patternBuilder = new StringBuilder("((?=.*[a-z])");

        if (forceSpecialChar)
        {
            patternBuilder.append("(?=.*[!#$%&'*+/=?`{|}~^])");
        }

        if (forceCapitalLetter)
        {
            patternBuilder.append("(?=.*[A-Z])");
        }

        if (forceNumber)
        {
            patternBuilder.append("(?=.*\\d+.*)");
        }

        patternBuilder.append(".{" + minLength + "," + maxLength + "})");
        Pattern pattern = Pattern.compile(patternBuilder.toString());
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }
}
