package Account;

/*
This class holds the information on the user that is currently logged into the booking system
 */
public class UserAccount {
    private static UserAccount Instance = null;
    private String _email;
    private String _name;
    private String _createTime;
    private String _lastAccessed;

    //Called by Database.java to create an instance of this Singleton class
    public static boolean login(String email, String name, String createTime, String lastAccessed){
        if (Instance == null){
            Instance = new UserAccount(email, name, createTime, lastAccessed);
            //If correct then login and fill detail fields
        }
        return false; //If false incorrect details
    }

    //Constructor
    private UserAccount(String email, String name, String createTime, String lastAccessed){
        _email = email;
        _name = name;
        _createTime = createTime;
        _lastAccessed = lastAccessed;
    }

    public static void logout(){
        Instance = null;
    }

    public static UserAccount getInstance() {
        return Instance;
    }

    public String getEmail() {
        return _email;
    }

    public String getName() {
        return _name;
    }

    public String getCreateTime() {
        return _createTime;
    }

    public String getLastAccessed() {
        return _lastAccessed;
    }
}
