package HtmlScraping;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {
    private static WebScrape _webScape;
    private static String _path = "http://www.empirecinemas.co.uk/?page=nowshowing&tbx_site_id=44&scope=";
    private static String _date = "04/12/2018";

    private static Movie _movieFirst;
    private static Movie _movieLast;

    @BeforeClass
    public static void setUp() throws Exception{
        _webScape = new WebScrape(_path, _date);
        _webScape.getPageElements();
        _webScape.createMovies();
        _webScape.setMovieClassifications();
        _webScape.setMovieTimes();

        _movieFirst = _webScape.getMovies().get(0);
        _movieLast = _webScape.getMovies().get(_webScape.getMovies().size() - 1);

        System.out.println(_movieFirst.getTitle());
        System.out.println(_movieLast.getTitle());
    }

    @Test
    public void getTitle() throws Exception{
        assertEquals("Creed II", _movieFirst.getTitle());
        assertEquals("The Possession Of Hannah Grace", _movieLast.getTitle());
    }

    @Test
    public void getDescription() throws Exception{
        assertEquals("Six years after the events of qWreck-It Ralphq, Ralph and Vanellope, now friends, discover a wi-fi router in their arcade, leading them into a new adventure.", _movieFirst.getDescription());
        assertEquals("When a cop who is just out of rehab takes the graveyard shift in a city hospital morgue, she faces a series of bizarre, violent events caused by an evil entity in one of the corpses.", _movieLast.getDescription());
    }

    @Test
    public void getClassification() throws Exception{
        assertEquals("Classification: PG", _movieFirst.getClassification());
        assertEquals("Classification: 15", _movieLast.getClassification());
    }

    @Test
    public void getTimes() throws Exception{
        assertTrue(_movieFirst.getShowdates().size() == 1);

        WebScrape nextDay = new WebScrape(_path, "05/12/2018");
        nextDay.getPageElements();
        nextDay.createMovies();
        nextDay.setMovieTimes();

        assertTrue(_movieFirst.getShowdates().size() == 2);

        assertEquals("[19:50, 11:20, 19:30, 09:50, 16:20, 13:50, 14:50, 16:50, 11:50, 12:20, 14:20, 17:20]", _movieFirst.getShowdates().get(0).getShowtimes().toString());
        assertEquals("[19:50, 11:20, 19:30, 09:50, 16:20, 13:50, 14:50, 16:50, 11:50, 12:20, 14:20, 17:20]", _movieFirst.getShowdates().get(1).getShowtimes().toString());
    }

    @Test
    public void getTrailerUrl() throws Exception{
        assertEquals("http://www.empirecinemas.co.uk/mm_xml/player_new.php?trailerid=16890&filmid=4202", _movieFirst.getTrailerUrl());
    }

    @Test
    public void getUrl() throws Exception{
        assertEquals("https://www.empirecinemas.co.uk/_uploads/film_images_small/8932_6405.jpg", _movieLast.getImageUrl());
    }

    @Test
    public void getRunningTime() throws Exception{
        assertEquals("Feature Running Time: 130 mins ", _movieFirst.getRunningTime());
    }

    @Test
    public void getWarnings() throws Exception{
        assertEquals("(Mild threat, rude humour.)", _movieFirst.getWarnings());
    }

}