package HtmlScraping;

import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class WebScrapeTest {
    private static WebScrape _webScrape;
    private static String _path = "http://www.empirecinemas.co.uk/?page=nowshowing&tbx_site_id=44&scope=";
    private static String _date = "03/12/2018";

    //@Before - Does before every test case
    //@Ignore ("Method not yet defined") - Allows ignoring of tests

    //@BeforeClass - does once, each test case shares the same variables
    @BeforeClass
    public static void setUp() throws Exception{
        _webScrape = new WebScrape(_path, _date);
    }

    @Test (expected = IllegalArgumentException.class)
    public void emptyConstructor() throws Exception{
        new WebScrape("", "");
    }

    @Test
    public void getUrl() throws Exception{
        assertEquals(_path + _date, _webScrape.getUrl());
    }

    @Test
    public void getDate() throws Exception
    {
        assertEquals("03/12/2018", _webScrape.getDate());
    }

    @Test
    public void getWebsiteName() throws Exception{
        _webScrape.getPageElements();
        assertEquals("EMPIRE CINEMAS Listings for Ipswich", _webScrape.getPage().getTitleText());
    }

    @Test
    public void createMovies() throws Exception{
        _webScrape.getPageElements();
        _webScrape.createMovies();
        assertTrue(_webScrape.getMovies().size() > 0);
    }

    @Test
    public void setMovieClassifications() throws Exception{
        _webScrape.getPageElements();
        _webScrape.createMovies();
        _webScrape.setMovieClassifications();

        boolean classificationEmpty = false;
        for (Movie movie : _webScrape.getMovies()) {
            if (movie.getClassification() == null) classificationEmpty = true;
        }
        assertTrue(!classificationEmpty);
    }

    @Test
    public void getMovieDate() throws Exception{
        _webScrape.getPageElements();
        _webScrape.createMovies();
        _webScrape.setMovieTimes();

        boolean timeEmpty = false;
        for (Movie movie : _webScrape.getMovies()) {
            if (movie.getShowdates().isEmpty()) timeEmpty = true;
        }
        assertTrue(!timeEmpty);
    }

    /*@Ignore ("Method has been re-written to create Movie objects")
    @Test
    public void getMovieTitles() throws Exception{
        _webScrape.getPageElements();
        ArrayList<String> movieTitles = _webScrape.createMovies();
        assertEquals("Ralph Breaks The Internet - Wreck-It Ralph 2", movieTitles.get(0));
    }*/
}