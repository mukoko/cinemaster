package HtmlScraping;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

public class MovieCollectionTest {
    private static MovieCollection _collection;

    @BeforeClass
    public static void setUp() throws Exception{
        _collection = MovieCollection.getInstance();
    }

    @Test
    public void getTodaysDate() throws Exception{
        assertEquals("03/12/2018", _collection.getTodaysDate());
    }

    @Test
    public void getPastDate() throws Exception{
        DateTime dateTime = new DateTime().withTimeAtStartOfDay().minusDays(1);
        DateTimeFormatter dt = DateTimeFormat.forPattern("dd/MM/yyyy");

        _collection.addMoviesForDate(dateTime.toString(dt));
        assertEquals(false, _collection.getMovies().size() > 0);
    }

    @Test
    public void getWeekAhead() throws Exception{
        DateTime dateTime = new DateTime().withTimeAtStartOfDay().plusDays(7);
        DateTimeFormatter dt = DateTimeFormat.forPattern("dd/MM/yyyy");

        _collection.addMoviesForDate(dateTime.toString(dt));
        assertEquals(false, _collection.getMovies().size() > 0);
    }

    @Test
    public void getDayAhead() throws Exception{
        DateTime dateTime = new DateTime().withTimeAtStartOfDay().plusDays(1);
        DateTimeFormatter dt = DateTimeFormat.forPattern("dd/MM/yyyy");

        _collection.addMoviesForDate(dateTime.toString(dt));
        assertTrue(_collection.getMovies().size() > 0);
    }

    @Test
    public void addMoviesForDate() throws Exception{
        _collection.addMoviesForDate(_collection.getTodaysDate());
        assertTrue(_collection.getMovies().size() > 0);
    }

    @Test
    public void addMultipleMovieDates() throws Exception{
        int dateCount;
        _collection.addMoviesForDate(_collection.getTodaysDate());
        dateCount = _collection.getMovies().get(0).getShowdates().size();
        assertEquals(11, _collection.getMovies().size());
        _collection.addMoviesForDate("04/12/2018");
        assertEquals(14, _collection.getMovies().size());

        assertTrue(_collection.getMovies().get(0).getShowdates().size() > dateCount);
    }

    @Test
    public void getMovieNamesForDate() throws Exception{
        HashMap<String, ArrayList<String>> names = MovieCollection.getInstance().getMoviesForDate(DateTime.now().toString(DateTimeFormat.forPattern("dd/MM/yyyy")));

        for (String key : names.keySet()) {
            System.out.println(key + " " + names.get(key).toString());
        }

        assertTrue(names.size() > 0);
    }
}