package Backbone;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationTest {

    @Test
    public void emailValidation() throws Exception{
        assertTrue(Validation.emailValidation("r@gmail.com",6, 32));
    }

    @Test
    public void emailValidation1() throws Exception{
        assertFalse(Validation.emailValidation("r@gmail",6, 32));
    }

    @Test
    public void emailValidation2() throws Exception{
        assertFalse(Validation.emailValidation("rgmail.com",6, 32));
    }

    @Test
    public void emailValidation3() throws Exception{
        assertFalse(Validation.emailValidation("r@g.c",6, 32));
    }

    @Test
    public void nameValidation() throws Exception{
        assertTrue(Validation.nameValidation("Rochel Mukoko", 6, 32));
    }

    @Test
    public void nameValidation2() throws Exception{
        assertFalse(Validation.nameValidation("Rochel Mukoko2", 6, 32));
    }

    @Test
    public void passwordValidation() throws Exception{
        assertTrue(Validation.passwordValidation("baNanaN1!", true, true, true, 6, 32));
    }

    @Test
    public void passwordValidation2() throws Exception{
        assertFalse(Validation.passwordValidation("baaana1!", true, true, true, 6, 32));
    }

    @Test
    public void passwordValidation3() throws Exception{
        assertFalse(Validation.passwordValidation("baNanaN!", true, true, true, 6, 32));
    }

    @Test
    public void passwordValidation4() throws Exception{
        assertFalse(Validation.passwordValidation("baNanaN1", true, true, true, 6, 32));
    }

    @Test
    public void passwordValidation5() throws Exception{
        assertFalse(Validation.passwordValidation("baN1!", true, true, true, 6, 32));
    }
}