package Backbone;

import org.junit.Test;

import static org.junit.Assert.*;

public class EncryptionTest {
    String hashedPassword = "5f4dcc3b5aa765d61d8327deb882cf99";

    @Test
    public void getSecurePassword() throws Exception{
        byte[] salt = Encryption.getSalt();
        String password = Encryption.getSecurePassword("password", salt);
        System.out.println(password);
        assertTrue(password.length() > 0);
    }

    @Test
    public void getPassword() throws Exception{
        String password = Encryption.getPassword("password");
        System.out.println(password);
        assertNotNull(password);
    }

    @Test
    public void getPassword2() throws Exception{
        String password = Encryption.getPassword("password");
        assertEquals(hashedPassword, password);
    }
}