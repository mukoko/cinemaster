package Backbone;

import Account.UserAccount;
import org.junit.Test;
import static org.junit.Assert.*;

public class DatabaseTest {

    @Test
    public void connects() throws Exception{
        Database.getInstance();
        assertNotNull(Database.getInstance());
        Database.getInstance().closeConnections();
    }

    @Test
    public void insertUser() throws Exception{
        int result = Database.getInstance().insertNewUser( "rtn@gmail.com", "password", "Rochel");
        Database.getInstance().closeConnections();
        assertEquals(1, result);
    }

    @Test
    public void insertDuplicateUser() throws Exception{
        int r = Database.getInstance().insertNewUser("j@gmail.com", "pass", "joa");
        assertEquals(1, r);

        int result = Database.getInstance().insertNewUser( "j@gmail.com", "pass", "namo");
        Database.getInstance().closeConnections();
        assertEquals(-1, result);
    }

    @Test
    public void getUserLogin() throws Exception{
        boolean success = Database.getInstance().userLogin("rk@gmail.com", Encryption.getPassword("ra"));
        assertTrue(success);
    }

    @Test
    public void updateLastAccess() throws Exception{
        assertEquals(1, Database.getInstance().updateLastAccess("rtn@gmail.com"));
    }


}