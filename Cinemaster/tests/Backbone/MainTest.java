package Backbone;

import States.StateManager;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void openLogin() throws  Exception{
        new Main();
        assertEquals("Login", StateManager.getInstance().getStateStack().peek().getName());
    }
}