package States;

import org.junit.Test;

import java.awt.print.Book;

import static org.junit.Assert.*;

public class BookingStateTest {

    @Test
    public void validInput() throws Exception{
        String testValue = "1";
        BookingState b = new BookingState(null);
        b._validInputRange = 4;
        assertTrue(b.validInput(testValue));
    }

    @Test
    public void validInput1() throws Exception{
        String testValue = "5";
        BookingState b = new BookingState(null);
        b._validInputRange = 4;
        assertFalse(b.validInput(testValue));
    }

    @Test
    public void validInput3() throws Exception{
        String testValue = "1aas";
        BookingState b = new BookingState(null);
        b._validInputRange = 4;
        assertFalse(b.validInput(testValue));
    }
}