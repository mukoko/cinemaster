package States;

import HtmlScraping.Movie;
import HtmlScraping.MovieCollection;
import HtmlScraping.WebScrape;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieSynopsisStateTest {

    @Test
    public void showTrailer() {
        MovieCollection.getInstance().addMoviesForDate(DateTime.now().toString(DateTimeFormat.forPattern("dd/MM/yyyy")));
        Movie movie = MovieCollection.getInstance().getMovies().get(0);
        new MovieSynopsisState(movie.getTitle()).showMedia(movie.getTrailerUrl(), movie.getTitle(), 700, 460);
    }

    @Test
    public void logout(){
        MovieCollection.getInstance().addMoviesForDate(DateTime.now().toString(DateTimeFormat.forPattern("dd/MM/yyyy")));
        Movie movie = MovieCollection.getInstance().getMovies().get(0);
        new MovieSynopsisState(movie.getTitle()).logout();
        assertEquals("Login", StateManager.getInstance().getStateStack().peek().getName());
    }
}