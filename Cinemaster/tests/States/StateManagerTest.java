package States;

import HtmlScraping.MovieCollection;
import org.junit.Test;

import static org.junit.Assert.*;

public class StateManagerTest {
    @Test
    public void openState() throws Exception{
        EntryState.open();
        assertEquals(1, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void closeState() throws Exception{
        EntryState.open();
        EntryState.close();
        assertEquals(0, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void openMultipleStates() throws Exception{
        EntryState.open();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        assertEquals(2, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void openAndCloseStates() throws Exception{
        EntryState.open();
        EntryState.close();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        assertEquals(1, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void closeMultipleStates() throws Exception{
        EntryState.open();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        NowShowingState.close();
        assertEquals(1, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void closeMultipleStates2() throws Exception{
        EntryState.open();
        EntryState.close();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        NowShowingState.close();
        assertEquals(0, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void closeAllStates() throws Exception{
        StateManager.getInstance().closeAllStates();
        assertEquals(0, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void closeAllStates2() throws Exception{
        EntryState.open();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        StateManager.getInstance().closeAllStates();
        assertEquals(0, StateManager.getInstance().getStateStack().size());
    }

    @Test
    public void closeAllStates3() throws Exception{
        EntryState.open();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        StateManager.getInstance().closeAllStates();
        NowShowingState.open(MovieCollection.getInstance().getTodaysDate());
        assertEquals(1, StateManager.getInstance().getStateStack().size());
    }
}
