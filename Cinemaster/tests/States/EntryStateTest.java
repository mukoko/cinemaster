package States;

import org.junit.Test;

import static org.junit.Assert.*;

public class EntryStateTest {

    @Test
    public void openLoginState() throws Exception{
        EntryState.open();
        assertEquals("Login", StateManager.getInstance().getStateStack().peek().getName());
    }
}